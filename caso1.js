var Foo = function( a ) {
  var baz = function() {
    return a;
  };
  this.baz = function() {
    return a;
  };
  this.bar = function() {
    return a;
  };
};

Foo.prototype = {
  biz: function() {
    return this.baz();
  }
};

var f = new Foo( 7 );
f.bar();
f.baz();
f.biz();
