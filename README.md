palindrome.js se ejecuta con:

node palindrome.js




a. What is a Promise?

b. What is ECMAScript?
Es una especificacion estandarizada de javascript. Basicamente agrega nueva funcionalidad y nuevas sintaxis para escribir complicadas funcionalidades (clases, modulos, iteradores, math, arrays, colecciones, promesas, proxies, entre otros).
Para logral retrocompatibilidad, se suele utilizar transpiladores como babel u otros.



c. What is NaN? What is its type? How can you reliably test if a value is equal to NaN ?
NaN quiere decir Not A Number value. Vendria a ser la version numerica de undefined o imposible de representar. Divisiones del estilo 0/0 y otras operaciones pueden resultar en NaN.
Se puede validar si un valor es igual a NaN con la funcion isNaN(), de resultado boolean.




d. What will be the output when the following code is executed? Explain.

console.log(false=='0') // true
console.log(false==='0') // false

el == compara si un valor es igual a otro en terminos de su valor. El integer 0, al ser convertido a boolean, es considerado como FALSO (al igual que el string "0" y el string vacio "").

el === compara igualdad de valor y tipo! Nos va a dar falso porque boolean no es igual a string.




Node

Explain what does "event-driven, non-blocking I/O" means in Javascript.

Node es single threaded, pero puede soportar concurrencia mediante eventos y callbacks
https://www.tutorialspoint.com/nodejs/nodejs_event_loop.htm

non blocking i/o
readFileSync es blocking y si el archivo es grande... hace lio porque cuelta toda la ejecucion del programa hasta que termine de leerse.
readFileAsync no bloquea la ejecucion y mediante un callback o una promise (si se promisifiquea) se puede trabajar con el contenido del archivo una vez terminada su lectura.
