var endorsements = [
  { skill: 'css', user: 'Bill' },
  { skill: 'javascript', user: 'Chad' },
  { skill: 'javascript', user: 'Bill' },
  { skill: 'css', user: 'Sue' },
  { skill: 'javascript', user: 'Sue' },
  { skill: 'html', user: 'Sue' }
];

// expected
// [
//   { skill: 'css', users: [ 'Bill', 'Sue', 'Sue' ], count: 2 },
//   { skill: 'javascript', users: [ 'Chad', 'Bill', 'Sue' ], count: 3 },
//   { skill: 'html', users: [ 'Sue' ], count: 1 }
// ]


var sortedEndorsements = [];

// recorre el array
for ( let i = 0; i < endorsements.length; i++ ) {
  let position = 0;
  let inserted = false;

  // buscamos si el skill de endorsements[i] es igual al skill en sortedEndorsements[position], siempre y cuando la posicion sea menor a sortedEndorsements.length
  while ( position < sortedEndorsements.length ){
    if ( sortedEndorsements[position].skill === endorsements[i].skill ){
      // si es igual, agrega el usuario e incrementa el count.
      // Posible mejora: chequear si el usuario ya existe en la lista
      sortedEndorsements[position].users.push(endorsements[i].user);
      sortedEndorsements[position].count++;
      inserted = true;
      break;
    } else {
      position++;
    }
  }
  // si no se inserto nada aun en sortedEndorsements, insertamos el objeto entero
  if (!inserted) {
    sortedEndorsements.push( { skill : endorsements[i].skill, users: [endorsements[i].user], count: 1 } );
  }
}

console.log(endorsements);
console.log('=====');
endorsements = sortedEndorsements; // hago este assign porque el ejercicio solicitaba que endorsements sea el array que resulte ordenado.
console.log(sortedEndorsements);
