
// inicializa standard in y standard out para lectura escritura en la consola
function prompt(question, callback) {
    let stdin = process.stdin;
    let stdout = process.stdout;

    stdin.resume();
    stdout.write(question);

    stdin.once('data', function (data) {
        callback(data.toString().trim());
    });
}

// funcion que chequea si el string ingresado es un palindrome. Funciona comparando el primer caracter (head)
// con el ultimo (tail). Si son iguales, avanza una posicion de head y retrocede una de tail, para repetir el proceso.
function isPalindrome(word){

    word = word.toLowerCase();
    var head = 0;
    var tail = word.length - 1;

    while (tail > head) {
      if (word[head] !== word[tail]) {
        return false;
      }
      head++;
      tail--;
    }
    return true;
};

// funcion para solicitar y recibir un string ingresado por el usuario
function askString () {
  console.log('');

  prompt('Please type your string: ', function (decision) {

    if (isPalindrome(decision)) {
      console.log('');
      console.log(' the string: ' + decision + ' is a palindrome !');
      console.log('');
      askString();
    } else {
      console.log('');
      console.log(' the string: ' + decision + ' is not a palindrome.');
      console.log('');
      askString();
    }
  });
}

console.log('');
console.log('');
console.log('');
console.log('  **********  Welcome to Palindrome Evaluator  **********');
console.log('');
console.log('    Enter a word or phrase and I will tell you if ');
console.log('               its a palindrome or not');
console.log('');


askString();
