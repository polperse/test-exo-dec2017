var myObject = {
    foo : "bar",
    func:function() {
        var self = this;
        var temp = this;

        console.log("outer func: this.foo = "+this.foo);
        console.log("outer func: self.foo = "+self.foo);

        (function(){
            console.log("inner func: this.foo = " +this.foo);
            console.log("inner func: self.foo = " +self.foo);
        }());
    }
};

myObject.func();


/*
resultado:
el 3er console log tirara undefined porque this no existe dentro de la funcion sin nombre.
sin embargo, self es una variable global a la cual si tiene acceso y podra acceder a "this"
para imprimir "bar"
*/
